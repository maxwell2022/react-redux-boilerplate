import express from 'express';
import path from 'path';
import webpack from 'webpack';
import webpackConfig from '../../webpack.config.js';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

const app = express();

const compiler = webpack(webpackConfig);
app.use(webpackHotMiddleware(compiler));
app.use(webpackDevMiddleware(compiler, {
  noInfo: true,
  publicPath: webpackConfig.output.publicPath,
}));

// Load middleware
app.use(express.static('./dist'));


app.use('/', (req, res) => {
  res.sendFile(path.resolve('src/client/index.html'));
});

const port = 3000;

export default {
  start: () => {
    app.listen(port, error => {
      if (error) {
        throw error;
      }
      console.log(`Express server is now listening on port ${port}`);
    });
  },
};
