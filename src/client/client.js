import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import createStore from 'app/store';
import AppContainer from 'app/containers/App';

const store = createStore({});

render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById('root')
);
