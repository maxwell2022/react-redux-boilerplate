import { applyMiddleware, compose, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

const createStoreWithMiddleware = compose(
  applyMiddleware(thunk, logger())
)(createStore);

const reducers = (state) => state;

export default function (defaultStore = {}) {
  return createStoreWithMiddleware(reducers, defaultStore);
}
