import React, { PropTypes } from 'react';
import classNames from 'classnames';

const App = () => {
  const classes = classNames('container', 'fluid');
  return (
    <div className={classes}>
      Main React appliction Container
    </div>
  );
};

App.propTypes = {
  className: PropTypes.string,
};

export default App;
